from flask import Blueprint, g, make_response, session
from .resources.scripts import plot

bp = Blueprint("index", __name__)

@bp.route("/user")
def get_user():
    response = make_response(session["user"], 200)
    response.headers['Content-Type'] = 'application/json'
    return response

@bp.route("/plots")#/<run>/<file>/<proj>/<depth>/<is_error>/<coli>")
def get_plot_html(
    # run,
    # file,
    # proj,
    # depth,
    # is_error,
    # coli
):
    plot_html = plot.main(
        "",
        "Dose_x[-260;160]cm_y[-220;200]_z[-230;200]_(5.0x5.0x5.0cm3)",
        "y",
        0,
        "rel",
        1
    )
    response = make_response(plot_html)
    response.headers['Content-Type'] = 'text/html'
    return response
