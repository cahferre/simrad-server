from flask import Blueprint
import requests
from flask import request, session
import os

CLIENT_ID = os.getenv("CLIENT_ID")
CLIENT_SECRET = os.getenv("CLIENT_SECRET")

DEFAULT_API_ACCESS_URI = "https://auth.cern.ch/auth/realms/cern/api-access/token"
DEFAULT_AUTH_URI = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect"

bp = Blueprint("auth", __name__)

@bp.before_app_request
def validate_token():
    if request.method != 'OPTIONS':
        token = request.headers.get("Authorization")
        if token is None:
            return "Missing token", 401
        token = token.split(" ")[1]
        introspected_token = introspect_token(token)
        if not introspected_token["active"]:
            return "Invalid token", 401
        session["user"] = format_user_info(introspected_token)

def introspect_token(token):
    url = DEFAULT_AUTH_URI + "/token/introspect"
    data = {"token": token}
    auth = (CLIENT_ID, CLIENT_SECRET)
    print(CLIENT_ID)
    resp = requests.post(url, data=data, auth=auth)
    return resp.json()

def format_user_info(token):
    return {
        "name": token["name"],
        "username": token["username"],
        "email": token["email"],
        "roles": token["cern_roles"],
        "personId": token["cern_person_id"],
    }
