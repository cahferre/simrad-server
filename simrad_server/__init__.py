from flask import Flask
from dotenv import load_dotenv
from flask_cors import CORS
import os

def create_app(test_config=None):
    load_dotenv()

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.getenv("SECRET_KEY"),
    )

    if app.debug:
        load_dotenv(".env.local")
        print("Running in local environment")

    CORS(app)

    app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")

    from . import auth
    from . import index

    app.register_blueprint(auth.bp)
    app.register_blueprint(index.bp)

    # make url_for('index') == url_for('blog.index')
    # in another app, you might define a separate main index here with
    # app.route, while giving the blog blueprint a url_prefix, but for
    # the tutorial the blog will be the main index
    #app.add_url_rule("/", endpoint="index")

    return app