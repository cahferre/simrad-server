import re
import os
import sys
import math
import numpy as np
import argparse
import json
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import mpld3

def drawFromJson(run, filename):
    subdirectory = "simrad_server/resources/data/"+str(run)
    filename = f"{filename}.json"
    with open(subdirectory + filename, "r") as json_file:
        loaded_data = json.load(json_file)
    return(loaded_data)


def checkDepth(lo, hi, count, depth):
    if depth<lo or depth>hi:
        print("Selected depth on the axis exceeds the available range!")
        print("No plot available")
        sys.exit(0)
    bins = np.linspace(lo, hi, count + 1)
    if depth in bins:
        bin_no = np.where(bins == depth)[0][0]
    else:
        print(f"Depth {depth} unavailable. Choose one of:")
        print(bins)
        sys.exit(0)
    return(bin_no)


def roundMin(x):
    if x<=0: return x
    else:    return pow(10,math.floor(math.log10(x)))

def roundMax(x):
    if x<=0: return x
    else:    return pow(10,math.ceil(math.log10(x)))


def makePlot(loaded_data, proj, depth, is_error, number_of_coli):
    data = np.array(loaded_data["data_to_plot"])
    data *= int(number_of_coli)

    title = loaded_data["title"]
    a_name = loaded_data["a_name"]
    a = loaded_data["a"]
    a_lo = loaded_data["a_lo"]
    a_hi = loaded_data["a_hi"]
    b_name = loaded_data["b_name"]
    b = loaded_data["b"]
    b_lo = loaded_data["b_lo"]
    b_hi = loaded_data["b_hi"]
    c_name = loaded_data["c_name"]
    c = loaded_data["c"]
    c_lo = loaded_data["c_lo"]
    c_hi = loaded_data["c_hi"]

    if proj == 'x':
        x_lo, x_hi = c_lo, c_hi
        y_lo, y_hi = b_lo, b_hi
        x_name, y_name = c_name, b_name
        bin_no = checkDepth(a_lo,a_hi,a,depth)
        summed_data = data[bin_no, :, :]
    elif proj == 'y':
        x_lo, x_hi = c_lo, c_hi
        y_lo, y_hi = a_lo, a_hi
        x_name, y_name = c_name, a_name
        bin_no = checkDepth(b_lo,b_hi,b,depth)
        summed_data = data[:, bin_no, :]
    elif proj == 'z':
        x_lo, x_hi = b_lo, b_hi
        y_lo, y_hi = a_lo, a_hi
        x_name, y_name = b_name, a_name
        bin_no = checkDepth(c_lo,c_hi,c,depth)
        summed_data = data[:, :, bin_no]

    max_limit = roundMax(np.max(data))
    min_limit = max_limit*1e-9
    norm = LogNorm(vmin = min_limit,vmax=max_limit*1)

    cmap_colors = [
    '#7F00FF', '#5500ff', '#330099','#001f3f', '#004080', '#0070bf',
    '#0080ff', '#0099FF', '#00CCFF', '#009900','#00CC33','#66CC33','#93F600',
    '#ffff00', '#ff8000',  '#FF0000', '#CC0000','#990033'
    ]

    custom_cmap = plt.cm.colors.ListedColormap(cmap_colors)
    size = summed_data.shape
    y = size[0]
    x = size[1]

    x_centers = np.linspace(x_lo, x_hi, x, endpoint=False)
    y_centers = np.linspace(y_lo, y_hi, y, endpoint=False)
    x_centers += (x_centers[1] - x_centers[0]) / 2
    y_centers += (y_centers[1] - y_centers[0]) / 2

    X, Y = np.meshgrid(x_centers, y_centers)

    #plt.imshow(summed_data, cmap = custom_cmap , extent=(x_lo, x_hi, y_lo, y_hi), origin='lower', norm=norm) #cmap=plt.get_cmap('viridis', 18)
    fig, ax = plt.subplots()
    pcm = ax.pcolormesh(X, Y, summed_data, cmap='viridis', norm=norm)

    ## TESTING COLORBAR
    #divider = make_axes_locatable(ax)
    #cax = divider.append_axes("left", size="5%", pad=1.5)

    fig.colorbar(pcm)#, location='left')#, ax=ax, cax=cax)  # Dodajemy pasek kolorów

    ax.set_xlabel(x_name + ' [cm]')  # Dodajemy nazwę osi X
    ax.set_ylabel(y_name + ' [cm]')  # Dodajemy nazwę osi Y
    if is_error == 'no_error':
        ax.set_title(title + ', '+proj+ ' = ' +str(depth)+ ' cm')
    elif is_error == 'rel':
        ax.set_title('Plot title')
    elif is_error == 'abs':
        ax.set_title('Absolute error of: '+ title + ', '+proj+ ' = ' +str(depth)+ ' cm')
    ax.set_aspect('auto', adjustable='box')
    return fig, ax


def showPlot(fig):
    plt.show()

def getPlotHtml(fig):
    return mpld3.fig_to_html(fig)

def main(run,filename,proj,depth,is_error,number_of_coli):
    if is_error != 'no_error':
        if is_error == 'rel':
            filename = filename + '.rel.ERR'

        else:
            filename = filename + '.abs.ERR'
    loaded_data = drawFromJson(run, filename)
    fig, ax = makePlot(loaded_data, proj, depth, is_error, number_of_coli)

    #showPlot(fig)  # display plot
    return getPlotHtml(fig) # plot to html


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=" ")
    parser.add_argument("--run", help = "name of run/simulation, should be available in data folder")
    parser.add_argument("--file", help="filename, should be available in data/<run name>/")
    parser.add_argument("--proj", help="projection (x, y or z)")
    parser.add_argument("--depth", help="depth in cm")
    parser.add_argument(
        '--is_error',
        nargs='?',
        default='no_error',
        choices=['rel', 'no_error', 'abs'],
        help="Error plot option. Available 'rel' for relative error, 'no_error' for normal plotting, and 'abs' for absolute error."
    )
    parser.add_argument(
        '--coli',
        nargs='?',
        default=1,
        help="Number of collisions. Default = 1."
    )

    args = parser.parse_args()
    main(args.run, args.file, args.proj, float(args.depth), args.is_error, args.coli)
